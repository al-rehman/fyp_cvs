#include "cache.h"
#define KBLU  "\x1B[34m"
#define KNRM  "\x1B[0m"
/*
** 	cvs-log command display all the commits history with their 
** 	user and commiter information. It reads all the SHA of commits 
** 	from file .commit_hash.txt in in .dircache and reads the 
** 	contents one by one and display it with arrangement from recent
** 	to oldest.
*/


int main( int argc , char **argv )
{

	int flg = 0;
	if(argc > 1 && strcmp(argv[1],"--one-line") == 0)
	{
		flg = 1;
	}

	int fd;
	void *buf;
	char type[20];
	unsigned long size;
	fd=open(".dircache/.commit_hash.txt",O_RDONLY);
	if(fd<0)
		printf("No Log to Show");
	char sha[41];
	char sha1[20];
	int offset = lseek(fd,0,SEEK_END);
	while( offset != 0)
	{
		offset=lseek(fd,-40,SEEK_CUR);
		read(fd,sha,40);
		sha[40]='\0';
		if (get_sha1_hex(sha, sha1))
			usage("invalid sha");
		buf = read_sha1_file(sha1, type, &size);
		
		if (!buf)
			continue;
			
		int fd3 = open(".dircache/info/log.txt",O_CREAT|O_TRUNC|O_RDWR,0644);
		dup2(1,fd3);
		if(flg == 0)
		{
			printf("\n%s%s %s%s\n",KBLU,type,sha,KNRM);
			fflush(stdout);
			if (write(1, buf, size) != size)
				strcpy(type, "bad");
		}
		else
		{
			printf("%s %.7s %s",KBLU,sha,KNRM);
			
			char buf2[1000];
			bzero(buf2,1000); 
			char buf3[1000];
			bzero(buf3,1000);
			char * q;
			q = strchr(buf,'\n');
			q++;
			int size1 =  size - (int)(q-(char *)buf);
			while(strncmp(q,"committer",9) != 0)
			{
				q=strchr(q,'\n');
				q++;
				size1 =  size - (int)(q-(char *)buf);
			}
			q = strchr(q,'\n');
			q++;
			size1 =  size - (int)(q-(char *)buf)-1;
			char * p1 = strchr(q+1,'\n');
			
			(*p1) = '\0';
			printf("%.*s",size1,++q);


		}
		printf("\n");

		offset = lseek(fd,-40,SEEK_CUR);
	}
	return 0;
}

