#include "cache.h"
#include<dirent.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<string.h>
#include<unistd.h>
void add_file_to_cache(char * file);
void call_cvs_update(char ** arglist);
void add_dir_to_cache(DIR * fdir);
void show(char ** arg, int count);
int main(int argc, char ** argv)
{
	if(argc <= 1)
	{
		printf("\n\tusage cvs add <directory/file name>\n");
		exit(1);
	}
	for(int i=1 ; i< argc ; i++ )
	{
		struct stat s;
//		printf("loop for each dir\n");
		int k = stat(argv[i],&s);
		if( k == -1 )
		{
			perror("Error: ");
			exit(1);
		}
//		printf("%o\n", 0040000);
//		printf("%o\n", s.st_mode & 0170000);
//		printf("%d\n", (s)((int.st_mode & 0170000) == 0040000));


		if( (s.st_mode & 0170000) == 0040000 )
		{
			DIR * fdir;
			fdir = opendir(argv[i]);
			if(fdir == NULL)
			{
				perror("Eorror: ");
				exit(1);
			}
//			printf("adding dir to cache function calling\n");
			add_dir_to_cache(fdir);			
		}
		else if( ( s.st_mode & 0170000) == 0100000)
		{
		
			
//			printf("In file mode section\n");
			add_file_to_cache(argv[i]);
		}
	}
	return 0;
}

/*
int match(char * str)
{
	int reti;
	regex_t regex;
	char msgbuf[100];
	int fd12 = open(".dircache/info/exclude",O_RDONLY);
	while(read(fd12,msgbuff,100)>0)
	{
		reti = regcomp(&regex, msgbuff, 0);
		if (reti) 
		{
			fprintf(stderr, "Could not compile regex\n");
			exit(1);
		}

		reti = regexec(&regex, str , 0, NULL, 0);
		if (!reti) 
		{
			printf("Match\n");
		}
		else if (reti == REG_NOMATCH) 
		{
			printf("No match\n");
		}
		else 
		{
			regerror(reti, &regex, msgbuf, sizeof(msgbuf));
			fprintf(stderr, "Regex match failed: %s\n", msgbuf);
			exit(1);
		}
	}
}
*/

void add_file_to_cache(char * file)
{
//	printf(" add file to cache section\n");
	char ** arglist = (char **) malloc((sizeof(char*)*2));
	arglist[0] = (char *)malloc(strlen("cvs-update"));
	strncpy(arglist[0],"cvs-update",strlen("cvs-update"));
	arglist[1] = (char *)malloc(strlen(file));
	strncpy(arglist[1],file,strlen(file));
	call_cvs_update(arglist);
}
void add_dir_to_cache(DIR * fdir)
{
//	printf("in function add dir to cache\n");
	char ** arglist;
	arglist = (char ** )malloc (sizeof(char *)*2);
	arglist[0] = "cvs-update";
	int argcount = 2;
	struct dirent * d;
//	printf("in function add dir to cache while starting for each file\n");
	while((d=readdir(fdir))!=NULL)
	{
		if(d->d_name[0] != '.')
		{
//			printf("in function add dir to cache while starting for each file %s\n",d->d_name);

			char * file1 = (char*)malloc(sizeof(char) * strlen(d->d_name)+1);

			strncpy(file1,d->d_name,strlen(d->d_name));
			file1[strlen(d->d_name)] = NULL;
			arglist[argcount-1] = file1;

			argcount++;
			arglist = realloc(arglist,sizeof(char*)*argcount);
		}
	}

	arglist[argcount-1]=0;

//	show(arglist,argcount);

//	printf("in function add dir to cache calling cvs update\n",d->d_name);
	call_cvs_update(arglist);
}


void show(char ** arg, int count)
{
	for(int i=0 ;i< count-1; i++)
	{
		printf("%d - %s\n",i,arg[i]);
	}
}

void call_cvs_update(char ** arglist)
{
//	printf("in function call cvs update\n");
	int cpid = fork();
	if(cpid == 0)
	{
//		show(arglist,3);
//		printf("in function add dir cache calling execlp\n");

		execvp("cvs-update",arglist);

	}
	else
	{
//		printf("in function call cvs update calling wait\n");
		while(wait(NULL)>0);
	}
}

