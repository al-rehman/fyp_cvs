#include<stdio.h> 

int main(int argc,char ** argv)
{
	if(argc == 1)
	{
		execvp("cvs-help",NULL);
	}
	else
	{
		if(strcmp(argv[1],"help") == 0)
		{
			execvp("cvs-help",++argv);
		}
		else if(strcmp(argv[1],"cat") == 0)
		{
			execvp("cvs-cat",++argv);
		}
		else if(strcmp(argv[1],"diff") == 0)
		{
			execvp("cvs-diff",++argv);
		}
		else if(strcmp(argv[1],"write-tree") == 0)
		{
			execvp("cvs-write_tree",++argv);
		}
		else if(strcmp(argv[1],"read-tree") == 0)
		{
			execvp("cvs-read_tree",++argv);
		}
		else if(strcmp(argv[1],"commit") == 0)
		{
			execvp("cvs-commit",++argv);
		}
		else if(strcmp(argv[1],"init") == 0)
		{
			execvp("cvs-init",++argv);
		}
		else if(strcmp(argv[1],"log") == 0)
		{
			execvp("cvs-log",++argv);
		}
		else if(strcmp(argv[1],"update") == 0)
		{
			execvp("cvs-update",++argv);
		}
		else if(strcmp(argv[1],"status") == 0)
		{
			execvp("cvs-status",++argv);
		}
		else
		{
			printf("\n%s is not a command\n",argv[1]);
		}





	}
	
	return 0;
}
