#include"cache.h"






static int unpack(unsigned char *sha1)
{
	void *buffer;
	unsigned long size;
	char type[20];
	/*	here we read an object file from .dircache/objects/ directory
	**	read_sha1_file() function defined in cvs-read.c is used to read
	**	it returns the pointer to data present in memeory
	*/
	buffer = read_sha1_file(sha1, type, &size);
	// checking file readed or not
	if (!buffer)
		usage("unable to read sha1 file");
	//checking tree type
	if (strcmp(type, "tree"))
		usage("expected a 'tree' node");
	//reading entries of file from buffer
	while (size) 
	{
		
		int len = strlen(buffer)+1;
		unsigned char *sha1 = buffer + len;
		char *path = strchr(buffer, ' ')+1;
		unsigned int mode;
		if (size < len + 20 || sscanf(buffer, "%o", &mode) != 1)
			usage("corrupt 'tree' file");
		buffer = sha1 + 20;
		size -= len + 20;
		
		int fd = open(path,O_CREAT|O_TRUNC|O_RDWR,0644);
		
		void * buf;
		unsigned long size12;
		buf = read_sha1_file(sha1, type, &size12);
		
		write(fd,buf,size12);
		

//		printf("%o %s (%s)\n", mode, path, sha1_to_hex(sha1));
	}
	return 0;
}






int main(int argc, char ** argv)
{
	
	if(argc <= 1)
	{
		printf("usage\n\tcvs checckout <commit-sha>\n");
		exit(1);
	}
	void *buf;	
	char type[20];
	char sha1[20];
	char * sha = argv[1];
	unsigned long size;
	
	if (get_sha1_hex(sha, sha1))
		usage("invalid sha");

	buf = read_sha1_file(sha1, type, &size);

	
	char buf2[1000];
	bzero(buf2,1000); 
	char buf3[1000];
	bzero(buf3,1000);
	char * q;
	q = buf;
	int size1 =  size - (int)(q-(char *)buf);
//	printf("%d\n",size1);

	q+=5;
	size1 =  40;
//	printf("%d\n",size1);

//	printf(" %.7s\n",sha);
//	printf("%.*s",size1,++q);
	char treebuf[41];
	strncpy(treebuf,q,40);
	treebuf[40]=0;
	
	system("rm *");

	if (get_sha1_hex(treebuf, sha1) < 0)
		usage("read-tree <key>");
	
	if (unpack(sha1) < 0)
		usage("unpack failed");	
	
	
	
	return 0;
}
