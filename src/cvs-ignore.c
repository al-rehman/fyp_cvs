#include "cache.h"


int main(int argc, char ** argv)
{
	if(argc <= 1)
	{
		printf("invalid number of arguments\n\tusage cvs ignore <filename/regex>...");
		exit(1);
	}
	int fd = open(".dircache/info/exclude",O_CREAT|O_APPEND|O_RDWR);
	if(fd<0)
	{
		printf("Error opening file\n");
		exit(1);
	}
	
	for( int i = 0; i < argc-1 ; i++ )
	{
		write(fd,argv[i+1],strlen(argv[i+1]));
		write(fd,"\n",1);
	}
}
