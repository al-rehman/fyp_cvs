#include "cache.h"
#include<dirent.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
static int cache_name_compare(const char *name1, int len1, const char *name2, int len2)
{
	int len = len1 < len2 ? len1 : len2;
	int cmp;

	cmp = memcmp(name1, name2, len);
	if (cmp)
		return cmp;
	if (len1 < len2)
		return -1;
	if (len1 > len2)
		return 1;
	return 0;
}


int main()
{
	char dir[250];
	getcwd(dir,250);
	DIR * fd = opendir(dir);
	struct dirent * r=readdir(fd);
	if(errno!=0)
	{
		perror("error: ");
		exit;
	}
	int in_right_dir = 0;
	while(r!=NULL)
	{
		if(strcmp(r->d_name, ".dircache") == 0)
		{
			in_right_dir = 1;
			break;
		}
		r=readdir(fd);
	}
	closedir(fd);



	if(in_right_dir == 1)
	{
		unsigned char sha1[41];
		int fd1 = open(".dircache/HEADS/master",O_RDONLY);
		if(fd1<0)
		{
			printf(".dircache/HEADS/master files not exist\n");
		}
		int k=read(fd1,sha1,40);
		if(k != 40)
		{
			printf("No commit Still Exist\n");
	
		}
		else
		{
			sha1[40]='\0';
			printf("We are at branch master\nThe Most recent commit SHA is \n\t%s\n",sha1);
		}

		int entries = read_cache();
		fd=opendir(dir);
		r=readdir(fd);
		
		
		int last = entries;
		

		
		char ** untracked;
		int u1 =0,u1lim=10;
		untracked  = (char **)malloc(sizeof(char*)*10);
		char ** modified;
		int m1 = 0,m1lim=10;
		modified = (char **)malloc(sizeof(char*)*10);
		while(r!=NULL)
		{
		
			if( r->d_name[0] == '.')
			{
				r=readdir(fd);
				continue;
			}
			int k1 = 0;
			struct cache_entry * ce;
			int found = 0;
			for( k1=0 ; k1 < last ; k1++)
			{
				ce = active_cache[k1];
				int cmp = cache_name_compare(r->d_name, strlen(r->d_name), ce->name, ce->namelen);
				if (!cmp)
				{
					found = 1;
					break;
				}
			}
			if(found == 1)
			{
				struct stat st;
				stat(r->d_name,&st);
				if(st.st_ctime != ce->ctime.sec)
				{
					modified[m1++] = r->d_name;
					if(m1 == m1lim)
					{
						m1lim +=10;
						modified = (char **)realloc(modified,sizeof(char*)*m1lim);
					}
				}
			}
			else
			{
				untracked[u1++] = r->d_name;
				if(u1 == u1lim)
				{
					u1lim +=10;
					modified = (char **)realloc(modified,sizeof(char*)*u1lim);
				}
			}
			


			r=readdir(fd);
		}
		closedir(fd);
		
		
		printf("Modifed Files\n");
		int m_i ;
		for(m_i =0 ; m_i <m1 ;m_i++)
		{
			printf("\t\t%s\n",modified[m_i]);
		}
		printf("Untracked Files\n");
		for(m_i =0 ; m_i <u1 ;m_i++)
		{
			printf("\t\t%s\n",untracked[m_i]);
		}




	}
	else
	{
		printf(".dircache not found\n");
	}
	return 0;
}

